function checkpass() {
  var str1 = String(document.querySelector(".pass").value);
  var str2 = String(document.querySelector(".c-pass").value);
  var fail = document.querySelector("#show");
  if (str1 == str2) {
    document.getElementById("show").innerHTML ='<i class="fa-solid fa-check" style="margin-right:5px"></i> Password Matched';
    fail.style.color = "green";
  } else {
    document.querySelector("#show").innerHTML = '<i class="fa-solid fa-circle-exclamation" style="margin-right:5px"></i>Password not Matched';
    fail.style.color = "red";
  }
}

function ValidPass() {
  var fail = document.querySelector("#show");
  if (
    document.querySelector(".pass").value != "" &&
    document.querySelector(".c-pass").value != ""
  ) {
    document.getElementById("show").innerHTML = '<i class="fa-solid fa-check" style="margin-right:5px"></i> Successfully Submitted';
    fail.style.cssText += 'background-color:#6CD151; color:black;';
  } else {
    document.querySelector("#show").innerHTML = 
    '<i class="fa-solid fa-circle-exclamation" style="margin-right:5px"></i> Please Fill all the fields..';
    fail.style.cssText += 'background-color:#FD3939;'
  }
}

const togglePassword = document.querySelector("#togglePassword");
const toggleCPassword = document.querySelector("#toggleCPassword");
const password = document.querySelector(".pass");
const Cpassword = document.querySelector(".c-pass");
togglePassword.addEventListener("click", function() {
  const type =
    password.getAttribute("type") === "password" ? "text" : "password";
    password.setAttribute("type", type);
    this.classList.toggle("bi-eye");
});
toggleCPassword.addEventListener("click" , function(){
    const types =
    Cpassword.getAttribute("type") === "password" ? "text" : "password";
    Cpassword.setAttribute("type", types);
    this.classList.toggle("bi-eye");
});

// function ValidateEmail() {
//   var email = document.getElementById("mail").value;
//   var icon = document.getElementById("emailValidation");
//   var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;

//   if (!expr.test(email)) {
//     icon.classList.togg("bi-envelope-check")
//   }
//   else{
//     icon.classList.add(".active");
//   }
// }