function generate(){
    var chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
    var passwordLength = 8;
    var password = "";
    
    for (var i = 0; i<passwordLength; i++){
        var randomNumber = Math.floor(Math.random() * chars.length);
        password += chars.substring(randomNumber,randomNumber+1);
    }
    document.getElementById("password").value = password
    password.style.display = "none";
};



function checkValidCaptcha(){
    var str1 = String(document.getElementById('password').value);
    var str2 = String(document.getElementById('entered-captcha').value);


    var fail = document.querySelector(".failed");
    if(str1==str2){
        document.getElementById("success").innerHTML="Validated Successfully";
        fail.style.color = "green";
    }
    else{
        document.querySelector(".failed").innerHTML = "Please Enter Valid Captcha..."
        fail.style.color = "red";
    }
}