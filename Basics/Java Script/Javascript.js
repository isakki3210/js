        // Data Types
        // variables
        var no1=10;
        var name='isakki';
        //letters,numbers,underscore,dollarsign
        //case sensitive
        var h1="heading 1";
        var H1="heading 2";
        var n1=10;
        var n2=20;


        //styles of variables

        //parial case
        var MyName ="isakki";

        //camelcase
        var myName ;

        //underscore
        var my_name ;
          //  alert(MyName);

    //ARRAYSSS...
    var animal=["Lion","Tiger","Rabbit"];
    var no=[5,22,33,44,55,6,7,8,9,0];
    //alert(animal[2]);
    //dot operation
    no.push("HII");
     // alert(no.length);
      //alert(no.sort());


      //loops
      // for,while,foreach
      for(var i=0;i<5;i++){
        console.log(i);
      }
      //while
      var i1=10;
      while(i1<18){
          console.log(i1);
          i1++;
      }

      //for each
      var no3=[1,2,5,9];
      no3.forEach(function(no4){
        console.log(no4);
      }
      );
      //conditions
      var b1=10;
      var b2=23;
      if(b1==b2){   //operators= AND=&& 
        console.log("true");
      }
      else{
        console.log("FALSE");
      }


      //switch
      var color="";
      switch(color){
        case"red":console.log("i like");
        break;
        case"blue":console.log("iiii");
        break;
        default:console.log("choose...");
        break;
      }

      //objects
      //object literals
      var person={
        fname:"isakki",
        lname:"muthu..",
        age:25,
        //embaded objects
        address:{
          street:2,
          city:"tuticorin",
          state:"TN",
        },
      }
 console.log(person.fname+" "+person.lname+"  " +person.address.city);
 //object constructor
   var isakki=new Object();
   isakki.name1="tiger";
   isakki.color="orange";
   console.log(isakki.color);



   //function with objects
   function isakki5(name5,color5){
     this.name1=name5;
     this.color=color5;
   }
   var tiger11=new isakki5("tiger","killer")
   var rabbit11=new isakki5("rabbit","white")
   console.log(tiger11)

